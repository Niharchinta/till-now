/**
 * Created by brtindia on 13-07-2016.
 */

var myapp = angular.module("myapp", ["ngRoute", "checklist-model"]).config(['$locationProvider', '$routeProvider',
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.
    when('/', {
      templateUrl: 'default.html',
      controller: 'defaultctrl'
    }).
    when('/view1', {
      templateUrl: 'view1.html',
      controller: 'view1ctrl'
    }).
    when('/view2', {
      templateUrl: 'view2.html',
      controller: 'view2ctrl'
    }).
    when('/last', {
      templateUrl: 'lastpage.html'

    }).
    otherwise('/');
  }
]).controller('defaultctrl', function ($http, $scope, $rootScope, $location, $window) {
  $rootScope.details = {"name": "", "projectdetails": "", "description": ""}
}).controller('view1ctrl', function ($rootScope, $http, $scope) {
  $http({
    method: "GET",
    url: "http://localhost:3000/api/Features"
  }).then(function mySucces(response) {
    $rootScope.myWelcome = response.data;
  }, function myError(response) {
    $scope.myWelcome = response.statusText;
  });
  $rootScope.user = {
    roles: []
  }
  $scope.myFunction2 = function(id) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
    //var elements = document.getElementsByClassName("dropdown-content");
    //for(var ele in elements) {
    //  var element = elements[ele];
    //  element.style = "";
    //}
    document.getElementById(id).classList.toggle("show");
  }
}).controller('view2ctrl', function ($rootScope, $http, $scope) {
  $http({
    method: "GET",
    url: "http://localhost:3000/api/UITemplates"
  }).then(function mySucces(response) {
    $rootScope.myWelcome = response.data;
  }, function myError(response) {
    $scope.myWelcome = response.statusText;
  });
  $rootScope.photo = {
    samples: []
  }
});

